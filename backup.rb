#!/usr/bin/ruby

LOG_FILE="/tmp/cron.log"
LOG=""
DIR=["dev","uni","docs","caratucay","ebooks"]
DISK="5d86d542-bd42-4695-bc7f-9908ab05c0b8"
MOUNTPOINT = "/media/disk_by-uuid_#{DISK}/"

def backup
  LOG << %x(pmount /dev/disk/by-uuid/#{DISK})
  # until File.exists? MOUNTPOINT
  #   sleep 1
  # end
  DIR.each do |d|
    LOG << %x(rsync -av $HOME/#{d} /media/disk_by-uuid_#{DISK}/backup/)
  end
  LOG << %x(pumount MOUNTPOINT)

  file = File.new(LOG_FILE, "w")
  file.write(LOG)
  file.flush
  file.close
end

backup
