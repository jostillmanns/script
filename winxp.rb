#!/usr/bin/ruby

require 'subtle/subtlext'

HIGH = "1920x1040"
LOW = "1366x768"

if Subtlext::Screen.current == Subtlext::Screen[0]
  system("rdesktop -K -u windows -p pass -g #{HIGH} -r sound:local 192.168.1.24")
else
  system("rdesktop -K -u windows -p pass -g #{LOW} 192.168.1.24")
end
